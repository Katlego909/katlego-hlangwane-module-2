void main() {
  var winningApps = [
    "FNB",
    "Snapscan",
    "Live Inspect",
    "Wumdrop",
    "Domestly",
    "Shyft",
    "Khula ecosystems",
    "Naked Insurance",
    "EasyEquities",
    "Ambani",
  ];

  for (var app in winningApps) {
    print("The name of the winning app is $app");
  }

  print('\n');

  print("The winning app of 2017 is ${winningApps[5]}");
  print("The winning app of 2018 is ${winningApps[6]}");

  print('\n');

  print("Total number of apps is ${winningApps.length}");
}
