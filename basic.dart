void main() {
  String name = "Katlego Hlangwane";
  String favoriteApp = "Twitter";
  String city = "Pretoria";

  print('My name is $name');
  print('My favorite app is $favoriteApp');
  print('I live in the city of $city');

  print('\n');

  print(
      "My name is $name, My favorite app is $favoriteApp and I live in the city of $city.");
}
